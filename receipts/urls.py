from django.urls import path
from receipts.views import main_page, contact_us, successView


urlpatterns = [
    path("", main_page, name="main_page"),
    path("contact/", contact_us, name="contact"),
    path("success/", successView, name="success"),

]
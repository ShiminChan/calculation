from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse
from receipts.forms import ContactForm

def main_page(request):
    return render(request, "receipts/main.html", {})

def contact_us(request):
    if request.method == "GET":
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            subject = form.cleaned_data["subject"]
            from_email = form.cleaned_data["from_email"]
            message = form.cleaned_data["message"]
            try:
                send_mail(subject, message, from_email, ["nat.jebon@gmail.com"])
            except BadHeaderError:
                return HttpResponse("Invalid header found.")
            return redirect("success")
            
    return render(request, "receipts/contact.html", {"form": form})

def successView(request):
    return render(request, "receipts/success.html", {})
